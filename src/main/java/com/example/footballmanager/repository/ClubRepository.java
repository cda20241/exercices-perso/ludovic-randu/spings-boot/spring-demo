package com.example.footballmanager.repository;
import com.example.footballmanager.entity.ClubEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClubRepository extends JpaRepository<ClubEntity,Long> {
    // la class JoueurRepository hérite de JpaRepository
    // JpaRepository fournit déjà toutes les fonctions les plus courantes de requêtes à la base de données
    // la fonction findById(), findAll(), create() etc...
    // la plupart du temps on n'a donc rien à faire de plus dans le repository
    // on doit faire 1 repository par class entity
}