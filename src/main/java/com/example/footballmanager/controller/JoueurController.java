package com.example.footballmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.footballmanager.repository.JoueurRepository;
import com.example.footballmanager.entity.JoueurEntity;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/joueur")
public class JoueurController {
    // on utilise l'annotation Autowired ici pour déclarer que joueurRepository sera injecté
    @Autowired
    private  final JoueurRepository joueurRepository;

    public JoueurController(JoueurRepository joueurRepository) {
        // C'est Spring qui se charge d'instancier la classe JoueurController, et en le faisant il voit
        // qu'il doit aussi créer une instance de JoueurRepository et nous la passer ici
        // ça nous simplifie la vie plutôt que de devoir créer nous-même l'instance
        this.joueurRepository = joueurRepository;
    }

    /**
     * Fonction du controller qui permet de recevoir la requête pour afficher tous les joueurs de la base de donnée.
     * @return une liste de Joueurs.
     */
    @GetMapping("/all")
    public List<JoueurEntity> getAllJoueurs() {
        return joueurRepository.findAll();
    }

    /**
     * Fonction du controller qui permet de recevoir la requête pour afficher un joueur selon son id.
     * @param id
     * @return Une instance Joueur
     */
    @GetMapping("/{id}")
    public JoueurEntity getJoueur(@PathVariable Long id) {
        Optional<JoueurEntity> joueur = joueurRepository.findById(id);
        if (joueur.isPresent()) {
            return joueur.get();
        }
        else {
            return null;
        }
    }


    /**
     * Fonction du controller qui permet de recevoir la requête pour ajouter un joueur.
     * @param joueur
     * @return
     */
    @PostMapping("")
    public ResponseEntity<String> addJoueur(@RequestBody JoueurEntity joueur){
        // On set l'id a nul pour ne pas remplacer de joueur, car on ne fait pas un update ici
        joueur.setId(null);
        joueur = this.joueurRepository.save(joueur);
        return new ResponseEntity<>("Welcome to " + joueur.getPrenom() + " " + joueur.getNom() + ".", HttpStatus.CREATED);
    }

    /**
     * Fonction du controller qui permet de recevoir la requête pour modifier un joueur.
     * @param id
     * @param joueur
     * @return une instance de Joueur modifiée
     */
    @PutMapping("/{id}")
    public JoueurEntity updateJoueur(@PathVariable Long id, @RequestBody JoueurEntity joueur) {
        return this.joueurRepository.save(joueur);
    }

    /**
     * Fonction du controller qui permet de recevoir la reqûete pour supprimer un joueur.
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteJoueur(@PathVariable Long id) {
        // ici on doit utiliser le type Optional car on n'est pas sûr que le joueur avec l'id demandé existe en bdd
        // auquel cas s'il n'existe pas le repository nous renverra null
        Optional<JoueurEntity> joueur = this.joueurRepository.findById(id);
        if (joueur.isPresent()) {
            this.joueurRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}


