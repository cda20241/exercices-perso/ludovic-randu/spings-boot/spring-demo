package com.example.footballmanager.controller;

import com.example.footballmanager.entity.ClubEntity;
import com.example.footballmanager.entity.JoueurEntity;
import com.example.footballmanager.repository.ClubRepository;
import com.example.footballmanager.repository.JoueurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/club")
public class ClubController {
    // on utilise l'annotation Autowired ici pour déclarer que joueurRepository sera injecté
    @Autowired
    private  final ClubRepository clubRepository;

    public ClubController(ClubRepository clubRepository) {
        // C'est Spring qui se charge d'instancier la classe JoueurController, et en le faisant il voit
        // qu'il doit aussi créer une instance de JoueurRepository et nous la passer ici
        // ça nous simplifie la vie plutôt que de devoir créer nous-même l'instance
        this.clubRepository = clubRepository;
    }

    /**
     * Fonction du controller qui permet de recevoir la requête pour afficher tous les joueurs de la base de donnée.
     * @return une liste de Joueurs.
     */
    @GetMapping("/all")
    public List<ClubEntity> getAllClub() {
        return clubRepository.findAll();
    }

    /**
     * Fonction du controller qui permet de recevoir la requête pour afficher un joueur selon son id.
     * @param id
     * @return Une instance Joueur
     */
    @GetMapping("/{id}")
    public ClubEntity getClub(@PathVariable Long id) {
        Optional<ClubEntity> club = clubRepository.findById(id);
        if (club.isPresent()) {
            return club.get();
        }
        else {
            return null;
        }
    }


    /**
     * Fonction du controller qui permet de recevoir la requête pour ajouter un joueur.
     * @param club
     * @return
     */
    @PostMapping("")
    public ResponseEntity<String> addClub(@RequestBody ClubEntity club){
        // On set l'id a nul pour ne pas remplacer de joueur, car on ne fait pas un update ici
        club.setId(null);
        club = this.clubRepository.save(club);
        return new ResponseEntity<>("Welcome to " + club.getNom() + " " + club.getVille() + ".", HttpStatus.CREATED);
    }

    /**
     * Fonction du controller qui permet de recevoir la requête pour modifier un joueur.
     * @param id
     * @param club
     * @return une instance de Joueur modifiée
     */
    @PutMapping("/{id}")
    public ClubEntity updateClub(@PathVariable Long id, @RequestBody ClubEntity club) {
        return this.clubRepository.save(club);
    }

    /**
     * Fonction du controller qui permet de recevoir la reqûete pour supprimer un joueur.
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteClub(@PathVariable Long id) {
        // ici on doit utiliser le type Optional car on n'est pas sûr que le joueur avec l'id demandé existe en bdd
        // auquel cas s'il n'existe pas le repository nous renverra null
        Optional<ClubEntity> club = this.clubRepository.findById(id);
        if (club.isPresent()) {
            this.clubRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}